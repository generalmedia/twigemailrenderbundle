<?php
namespace Generalmedia\TwigEmailRenderBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Created by PhpStorm.
 * User: laurent
 * Date: 28.12.15
 * Time: 16:41
 */
class TwigExtensionPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if(false === $container->hasDefinition("generalmedia_email_render_twig")){
            return;
        }
        $definition = $container->findDefinition(
            "generalmedia_email_render_twig"
        );

        $services = $container->findTaggedServiceIds("twig.extension");
        foreach($services as $id => $tags){
            $definition->addMethodCall("addExtension", [new Reference($id)]);
        }
    }
}