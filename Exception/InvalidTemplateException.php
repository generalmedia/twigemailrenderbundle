<?php
/**
 * Created by PhpStorm.
 * User: laurent
 * Date: 09.12.15
 * Time: 15:13
 */

namespace Generalmedia\TwigEmailRenderBundle\Exception;


class InvalidTemplateException extends \Exception
{
    /**
     * @var array
     */
    private $blocks;
    /**
     * @var string
     */
    private $template;

    public function __construct($template, array $blocks = [], \Exception $previous = null)
    {
        $message = sprintf("Your template %s must have blocks: %s", $template, implode(", ", $blocks));
        parent::__construct($message, 0, $previous);
        $this->blocks   = $blocks;
        $this->template = $template;
    }

    /**
     * @return array
     */
    public function getBlocks()
    {
        return $this->blocks;
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }
}