# Twig Email Render Bundle

Allow you to render Swiftmailer email using Twig Engine

[![Build Status](https://travis-ci.org/GeneralMediaCH/TwigEmailRenderBundle.svg?branch=master)](https://travis-ci.org/GeneralMediaCH/TwigEmailRenderBundle)

## Installation
Add this to your composer.json:

```json
  "require": {
	"generalmedia/twigemailrenderbundle" : "1.0.*"
   },
  "repositories": [
    {
      "type": "vcs",
      "url": "git@github.com:GeneralMediaCH/TwigEmailRenderBundle.git"
    }
  ],
```

Run composer update.

Add this to you AppKernel:

```php
new Generalmedia\TwigEmailRenderBundle\TwigEmailRenderBundle(),
```


## Usage

Render email like twig views:

```php
$msg = $this->get("generalmedia_email_render_twig")->getMessage("MyTemplate.email.twig");
$msg->setTo("...@...");
$this->get("mailer")->send($msg);
```

Your template is rendered with 3 blocks:

- Block *subject*: Your email subject
- Block *text* : Your email text
- Block *html:* Your email html.

Subject is mandatory, and you must define at least one of theses blocks: text, html.

## Configuration
All theses block names can be configured as follow:

```yaml
twig_email_render:
  blocks:
    subject: 'email_subject'
    text: 'email_text'
    html: 'email_html'

```

