<?php
/**
 * Created by PhpStorm.
 * User: laurent
 * Date: 09.12.15
 * Time: 15:12
 */

namespace Generalmedia\TwigEmailRenderBundle\Render;


use Generalmedia\TwigEmailRenderBundle\Exception\InvalidTemplateException;

interface EmailRenderInterface
{
    /**
     * Render a swift message with twig blocks
     * @param $template string template name
     * @param array $parameters
     * @param \Swift_Message $message
     * @return \Swift_Message
     * @throws InvalidTemplateException If template doesn't have the required blocks
     */
    public function getMessage($template, $parameters = array(), \Swift_Message $message = null);
}