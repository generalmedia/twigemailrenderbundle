<?php

namespace Generalmedia\TwigEmailRenderBundle\Render;

use Generalmedia\TwigEmailRenderBundle\Exception\InvalidTemplateException;
use Twig_LoaderInterface;

/**
 * Class EmailRenderService
 * Render a SwiftMessage using Twig engine
 * @package Generalmedia\TwigEmailRenderBundle\Render
 */
class EmailRenderService implements EmailRenderInterface
{
    const FORMAT_HTML = "html";
    const FORMAT_TEXT = "text";
    const FORMAT_SUBJECT = "subject";
    /**
     * @var \Twig_Environment Environment used for body tag
     */
    protected $twigHtml;

    /**
     * @var \Twig_Environment Environment used for subject and text block
     */
    protected $twigText;
    /**
     * @var \string[] array representing each blockName indexed by FORMAT_* key
     */
    private $blockConfig;

    /**
     * EmailRenderService constructor.
     * @param Twig_LoaderInterface $loader
     * @param string[] $blockConfig
     */
    public function __construct(Twig_LoaderInterface $loader, array $blockConfig = [])
    {
        // We use multiple environments to use different escape strategy per block
        $this->twigHtml = new \Twig_Environment($loader, ['autoescape' => 'html']);
        $this->twigText = new \Twig_Environment($loader, ['autoescape' => false]);
        /** @var \Twig_Extension_Escaper $escaper */
        $escaper = $this->twigText->getExtension('escaper');
        $escaper->setDefaultStrategy(false);


        $this->blockConfig = array_merge(["text" => "text", "html" => "html", "subject" => "subject"], $blockConfig);
    }

    public function addExtension(\Twig_Extension $extension)
    {
        $this->twigHtml->addExtension($extension);
        $this->twigText->addExtension($extension);
    }

    /**
     * Render a swift message with twig blocks
     * @param $template string template name
     * @param array $parameters
     * @param \Swift_Message $message
     * @return \Swift_Message
     * @throws InvalidTemplateException If template doesn't have the required blocks
     */
    public function getMessage($template, $parameters = array(), \Swift_Message $message = null)
    {
        $text    = $this->renderBlock($template, self::FORMAT_TEXT, $parameters);
        $html    = $this->renderBlock($template, self::FORMAT_HTML, $parameters);
        $subject = $this->renderBlock($template, self::FORMAT_SUBJECT, $parameters);

        // Throw exception if html/text is empty
        if (empty($html) && empty($text)) {
            $blockNames = [];
            foreach ([self::FORMAT_HTML, self::FORMAT_TEXT] as $format) {
                $blockNames[] = $this->getBlockName($format);
            }
            throw new InvalidTemplateException($template, $blockNames);
        }

        // Throw exception if subject is empty
        if (empty($subject)) {
            throw new InvalidTemplateException($template, [$this->getBlockName(self::FORMAT_SUBJECT)]);
        }

        // Create message
        if ($message === null) {
            $message = new \Swift_Message();
        }
        $message->setSubject($subject);
        if (!empty($html)) {
            $message->addPart($html, "text/html");
        }
        if (!empty($text)) {
            $message->setBody($text);
        }

        return $message;
    }

    /**
     * Render a block from a twig template
     * @param $template string Template name
     * @param $format string format
     * @param $parameters array Template context
     * @return string rendered text
     */
    protected function renderBlock($template, $format, $parameters)
    {
        $template = $this->getTwig($format)->loadTemplate($template);

        /** @var \Twig_Template $template */
        return $template->renderBlock($this->getBlockName($format), $parameters);
    }

    /**
     * Get the twig environment to use for a specified format
     * @param $format
     * @return \Twig_Environment
     * @throws \RuntimeException Unknown format
     */
    protected function getTwig($format)
    {
        switch ($format) {
            case self::FORMAT_HTML:
                return $this->twigHtml;
            case self::FORMAT_TEXT:
            case self::FORMAT_SUBJECT:
                return $this->twigText;
            default:
                throw new \RuntimeException("Unknown format ".$format);
        }
    }

    /**
     * Get the twig block name to use for a specified format
     * @param $format string Format to render
     * @return string Block name to render
     * @throws \RuntimeException Unknown format
     */
    protected function getBlockName($format)
    {
        if (!isset($this->blockConfig[$format])) {
            throw new \RuntimeException("Unknown format ".$format);
        }

        return $this->blockConfig[$format];
    }
}