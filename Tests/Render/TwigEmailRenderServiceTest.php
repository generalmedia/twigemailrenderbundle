<?php

namespace Generalmedia\TwigEmailRenderBundle\Tests\Render;

use Generalmedia\TwigEmailRenderBundle\Exception\InvalidTemplateException;
use Generalmedia\TwigEmailRenderBundle\Render\EmailRenderService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class TwigEmailRenderServiceTest extends \PHPUnit_Framework_TestCase
{
    /** @var EmailRenderService */
    private $emailRenderService;

    /** @var EmailRenderService */
    private $renamedEmailRenderService;

    /**
     * Test invalid templates
     */
    public function testRequiredBlocks()
    {
        $templates = [
            "no_block.email.twig",
            "only_body.email.twig",
            "only_subject.email.twig",
            "only_text.email.twig",
        ];
        foreach ($templates as $template) {
            try {
                $this->emailRenderService->getMessage($template);
                $this->assertTrue(false);
            } catch (InvalidTemplateException $iae) {
                $this->assertFalse(empty($iae->getBlocks()));
                $this->assertEquals($template, $iae->getTemplate());
            }
        }
    }

    /**
     * Test subject on a valid template
     * @throws InvalidTemplateException
     */
    public function testFullSubject()
    {
        $message = $this->emailRenderService->getMessage("full.email.twig");
        $this->assertEquals("à la volette", $message->getSubject());
    }

    /**
     * Test text on a valid template
     * @throws InvalidTemplateException
     */
    public function testFullText()
    {
        $message = $this->emailRenderService->getMessage("full.email.twig");
        $this->assertEquals("My text", $message->getBody());
    }

    /**
     * Test text on a valid template
     * @throws InvalidTemplateException
     */
    public function testText()
    {
        $message = $this->emailRenderService->getMessage("text.email.twig");
        $this->assertEquals(null, $this->getHtmlPart($message));
        $this->assertEquals("My text", $message->getBody());
        $this->assertEquals("à la volette", $message->getSubject());
    }

    /**
     * TODO Better support of RFC1341
     * @param \Swift_Message $message
     * @return null
     */
    protected function getHtmlPart(\Swift_Message $message)
    {
        $dumpedMessage = explode("\r\n", $message->toString());
        // save current line (relative to $key)
        $index = 0;

        foreach ($dumpedMessage as $key => $value) {
            if ($value != "Content-Type: text/html; charset=utf-8") {
                continue;
            }
            $index = 1;

            // Detect Transfert Encoding
            $encoding = null;
            while (true) {
                if (strpos($dumpedMessage[$key + $index], "Content-Transfer-Encoding:") !== false) {
                    $line     = trim($dumpedMessage[$key + $index]);
                    $encoding = substr($line, 27, strlen($line) - 27);
                    $encoding = explode(';', $encoding);
                    $encoding = strtolower($encoding[0]);
                    break;
                }
                if ($dumpedMessage[$key + $index] == "") {
                    $index++;
                    break;
                }
                $index++;
            };
            // Skip others header, to get content
            while ($dumpedMessage[$key + $index] !== "") {
                $index++;
            };

            $result = [];
            $last   = count($dumpedMessage);

            // Get all mime/part until boundary is reached
            while (strpos($dumpedMessage[$key + $index], $message->getBoundary()) === false) {
                $txt = $dumpedMessage[$key + $index];


                // Remove closing boundary
                if ($key + $index === $last - 1) {
                    $txt = substr($txt, 0, strlen($txt) - 4);
                }

                // Remove soft break-line
                $len = mb_strlen($txt);
                if ($encoding == "quoted-printable" && $len > 1 && substr($txt, -1) == "=") {
                    $txt = substr($txt, 0, $len - 1);
                }

                $result[] = $txt;
                $index++;
            }

            $resultstr = implode("", $result);

            switch ($encoding) {
                case 'quoted-printable':
                    return quoted_printable_decode($resultstr);
                case 'base64':
                    return base64_decode($resultstr);
                default:
                    return $resultstr;
            }
        }

        return null;

    }


    /**
     * Test all fields on a valid dynamic template
     * @throws InvalidTemplateException
     */
    public function testDynamicFull()
    {

        $subject = "éléàè".rand();
        $text    = "Bacon ipsum dolor amet pork chop beef ribs drumstick flank cupim,".
            " brisket biltong. Salami ham cupim turducken jerky pork sausage pork".
            "chop pancetta meatloaf kevin. Sausage porchetta cupim, capicola shankle ".
            "cow picanha jerky beef ribs doner. Beef ribs prosciutto t-bone alcatra ".
            "flank pork frankfurter. Short ribs cupim brisket, salami drumstick kevin ".
            "venison hamburger prosciutto cow.";

        $html = str_replace("Bacon", "<b>Bacon</b>", $text);

        $message = $this->emailRenderService->getMessage(
            "dynamic_full.email.twig",
            [
                "subject" => $subject,
                "text"    => $text,
                "html"    => $html,
            ]
        );
        $this->assertEquals($html, $this->getHtmlPart($message), " Html match");
        $this->assertEquals($text, $message->getBody());
        $this->assertEquals($subject, $message->getSubject());
    }

    /**
     * Test html on a valid template
     * @throws InvalidTemplateException
     */
    public function testFullHtml()
    {
        $message = $this->emailRenderService->getMessage("full.email.twig");
        $this->assertEquals("<b>My Body</b>", $this->getHtmlPart($message), "No html found in message");
    }

    /**
     * Test all fields on a valid template
     * @throws InvalidTemplateException
     */
    public function testHtml2()
    {
        $message = $this->emailRenderService->getMessage("html.email.twig");
        $this->assertEquals("<b>My Body</b>", $this->getHtmlPart($message), "No html found in message");
        $this->assertEquals(null, $message->getBody());
        $this->assertEquals("à la volette", $message->getSubject());
    }


    /**
     * Test all fields on a valid template
     * @throws InvalidTemplateException
     */
    public function testRenamedBlocks()
    {
        $message = $this->renamedEmailRenderService->getMessage("rename_blocks.email.twig");
        $this->assertEquals("<b>My Body</b>", $this->getHtmlPart($message), "No html found in message");
        $this->assertEquals("My text", $message->getBody());
        $this->assertEquals("à la volette", $message->getSubject());
    }

    /**
     * Instantiate service. Use a custom filesystem loader to find tests templates
     */
    public function setUp()
    {
        $loader                          = new \Twig_Loader_Filesystem(array(__DIR__.'/../Resources/Views/'));
        $this->emailRenderService        = new EmailRenderService($loader);
        $this->renamedEmailRenderService = new EmailRenderService(
            $loader,
            [
                "html"    => 'mail_html',
                "text"    => 'mail_plain_text',
                "subject" => 'mail_object',
            ]
        );
    }
}
