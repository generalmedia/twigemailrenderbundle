<?php

namespace Generalmedia\TwigEmailRenderBundle;

use Generalmedia\TwigEmailRenderBundle\DependencyInjection\Compiler\TwigExtensionPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class TwigEmailRenderBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new TwigExtensionPass());
    }

}
